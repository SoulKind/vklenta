package com.example.vklenta.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.vklenta.R
import kotlinx.android.synthetic.main.cart_news.view.*

class MainAdapter (val items: ArrayList<Int>) : RecyclerView.Adapter<MainAdapter.ViewHolder>(){


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.image.setImageResource(items[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.cart_news, parent, false)

        return ViewHolder(
            view
        )
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        var image = view.cartImage!!
    }

    override fun getItemCount(): Int {
        return items.count()
    }
}