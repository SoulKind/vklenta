package com.example.vklenta

import android.content.res.ColorStateList
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.example.vklenta.fragments.*

lateinit var toolbar: Toolbar
lateinit var messageFragment: MessageFragment
lateinit var moviesFragment: MoviesFragment
lateinit var newsFragment: NewsFragment
lateinit var profileFragment: ProfileFragment
lateinit var servicesFragment: ServicesFragment
var newsSelected: Boolean = true
var servicesSelected: Boolean = false
var messageSelected: Boolean = false
var moviesSelected: Boolean = false
var profileSelected: Boolean = false

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        newsFragment = NewsFragment()
        supportFragmentManager
            .beginTransaction()
            .add(R.id.fragment_container,  newsFragment)
            .commit()
        setColorMenuButton(R.id.buttonNews)
    }

    private fun setTextInView(text: String?){
        Toast.makeText(
            applicationContext,
            text,
            Toast.LENGTH_LONG
        ).show()
    }

    fun onClickMainMenuButton(view: View){
        when(view.id){
            R.id.buttonProfile ->{
                profileFragment = ProfileFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_container,  profileFragment)
                    .commit()
                setColorMenuButton(view.id)
            }
            R.id.buttonMovies ->{
                moviesFragment = MoviesFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_container,  moviesFragment)
                    .commit()
                setColorMenuButton(view.id)
            }
            R.id.buttonMessages ->{
                messageFragment = MessageFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_container,  messageFragment)
                    .commit()
                setColorMenuButton(view.id)
            }
            R.id.buttonNews ->{
                newsFragment = NewsFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_container,  newsFragment)
                    .commit()
                setColorMenuButton(view.id)
            }
            R.id.buttonServices ->{
                servicesFragment = ServicesFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_container,  servicesFragment)
                    .commit()
                setColorMenuButton(view.id)
            }
        }
    }

    private fun setColorMenuButton(id: Int){
        messageSelected = id == R.id.buttonMessages
        servicesSelected = id == R.id.buttonServices
        newsSelected = id == R.id.buttonNews
        moviesSelected = id == R.id.buttonMovies
        profileSelected = id == R.id.buttonProfile

        if(messageSelected)
        {
            val button = findViewById<ImageButton>(R.id.buttonMessages)
            button.imageTintList =
                ColorStateList.valueOf(ContextCompat.getColor(button.context, R.color.myTextWhite))
        }
        else
        {
            val button = findViewById<ImageButton>(R.id.buttonMessages)
            button.imageTintList =
                ColorStateList.valueOf(ContextCompat.getColor(button.context, R.color.myLightGrey))
        }

        if(servicesSelected)
        {
            val button = findViewById<ImageButton>(R.id.buttonServices)
            button.imageTintList =
                ColorStateList.valueOf(ContextCompat.getColor(button.context, R.color.myTextWhite))
        }
        else
        {
            val button = findViewById<ImageButton>(R.id.buttonServices)
            button.imageTintList =
                ColorStateList.valueOf(ContextCompat.getColor(button.context, R.color.myLightGrey))
        }

        if(newsSelected)
        {
            val button = findViewById<ImageButton>(R.id.buttonNews)
            button.imageTintList =
                ColorStateList.valueOf(ContextCompat.getColor(button.context, R.color.myTextWhite))
        }
        else
        {
            val button = findViewById<ImageButton>(R.id.buttonNews)
            button.imageTintList =
                ColorStateList.valueOf(ContextCompat.getColor(button.context, R.color.myLightGrey))
        }

        if(moviesSelected)
        {
            val button = findViewById<ImageButton>(R.id.buttonMovies)
            button.imageTintList =
                ColorStateList.valueOf(ContextCompat.getColor(button.context, R.color.myTextWhite))
        }
        else
        {
            val button = findViewById<ImageButton>(R.id.buttonMovies)
            button.imageTintList =
                ColorStateList.valueOf(ContextCompat.getColor(button.context, R.color.myLightGrey))
        }

        if(profileSelected)
        {
            val button = findViewById<ImageButton>(R.id.buttonProfile)
            button.imageTintList =
                ColorStateList.valueOf(ContextCompat.getColor(button.context, R.color.myTextWhite))
        }
        else
        {
            val button = findViewById<ImageButton>(R.id.buttonProfile)
            button.imageTintList =
                ColorStateList.valueOf(ContextCompat.getColor(button.context, R.color.myLightGrey))
        }
    }

    fun onClickToolBar(view: View) {
        when(view.id){
            R.id.buttonCamera -> setTextInView("buttonCamera")
            R.id.buttonNewsTool -> setTextInView("buttonNewsTool")
            R.id.buttonIntresting -> setTextInView("buttonIntresting")
            R.id.buttonNotification -> setTextInView("buttonNotification")
        }
    }
}