package com.example.vklenta.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.vklenta.R
import com.example.vklenta.adapters.MainAdapter


class NewsFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.news_recycler_view, container, false)
        val newsList = view.findViewById<RecyclerView>(R.id.newsRecyclerView)
        var llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        newsList.layoutManager = llm

        val imagesList: ArrayList<Int> = ArrayList()
        imagesList.add(R.drawable.one_image)
        imagesList.add(R.drawable.two_image)
        imagesList.add(R.drawable.free_image)

        newsList.adapter = MainAdapter(imagesList)

        return view
    }
}